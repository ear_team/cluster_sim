# Cluster Sim

This repository contains the source code for Cluster_sim, an EARD-cluster simulator, as well as a modified EARGM to interact with it and the necessary changes to Batsched's source to also communicate with Cluster_sim.

## Installation
Due to current requirements of EARGM, MySQL/MariaDB's C client library must be installed. If it is not found automatically, set the path in the DB_LDFLAGS variable in the main Makefile.

A current EAR version (4.3 or above) must be downloaded AND compiled. Set the EARDIR variable in the main Makefile to the path of the current EAR sources.

A recent Batsched version (using the main branch in their official repository) source code must be downloaded. Replace the files in Batsched's source by their equivalents on `src/batsched_files`.
A simple way is to:

```
    cp src/batsched_files/main.cpp /path/to/batsched/src/
    cp src/batsched_files/isalgorithm.* /path/to/batsched/src/
    cp src/batsched_files/easy_bf.* /path/to/batsched/src/algo/
    cp src/batsched_files/sequencer_dvfs.* /path/to/batsched/src/algo/
```

Compile Batsched. If using Nix, use `'nix-env -iA batsched -f /path/to/batsched'`. If compiling from source, follow the instructions on their site.

Compile EAR.

Once both Batsched and the main EAR folder have been compiled, simply `make`. This will compile all the executables on EAR's side. 

## Usage

To use Batsched, you will need to have Batsim installed, as well as a valid cluster definition and a workload trace. Once both Batsim and Batsched have been installed, a folder where Batsim will place the outputs must be created. Finally, a power profile must be given. A power profile can be generated with `./src/powergen_cluster_sim [num_jobs] [min power] [max power] [CPU perc] [MEM perc] [MIX perc]` or manually provided by the user.

To run the simulation on Batsched & Batsim:
```
batsim -p path/to/cluster_definition.xml --mmax-workload -w /path/to/workload.json -e /path/to/outs/ -E
batsched -v easy_bf --verbosity=debug
```

The simulators will then wait for Cluster_sim to start. Before Cluster_sim can start, an instance of EARGM must be started with `./src/global_manager/eargmd`. For more information, check the Global Manager page in EAR's wiki. 
Once EARGM is running, there are two environment variables that _should_ be set before running cluster_sim:
```
#set the number of EARDs to simulate
export CLUSTER_SIM_NUM_NODES=10000
#set their starting powercap
export CLUSTER_SIM_DEF_POWERCAP=750
```
Additionally, an extra environment variable `CLUSTER_SIM_STOP_NODES=0` can be set to control whether the scheduler should stop job submissions (1) or not (0), depending on the status of the cluster.

Finally, launch Cluster_sim with:
```
./src/cluster_sim [simulation_tag] /path/to/power/profile.txt
```
The simulation tag will only be used in the output trace file.

Once the simulation finished, two trace files will be generated: `traceXXX.csv` and `traceXXXsummary.csv`, where XXX is the unix timestamp of the end of the simulation. The first file contains a timeline where, at each event, all active jobs print their information, as well as aggregate of the information of all the idle nodes. The second one contains a per-job average of all the metrics, as well as the average of all the idle time on the cluster.


## Known issues
Sometimes, when creating the initial link between Cluster_sim and Batsched, they will hang. When that happens, stop Batsim, Batsched and Cluster_sim (*NOT* EARGM) and start them again.

