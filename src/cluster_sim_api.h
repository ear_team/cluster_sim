/*
 * *
 * * This program is part of the EAR software.
 * *
 * * EAR provides a dynamic, transparent and ligth-weigth solution for
 * * Energy management. It has been developed in the context of the
 * * Barcelona Supercomputing Center (BSC)&Lenovo Collaboration project.
 * *
 * * Copyright © 2017-present BSC-Lenovo
 * * BSC Contact   mailto:ear-support@bsc.es
 * * Lenovo contact  mailto:hpchelp@lenovo.com
 * *
 * * EAR is an open source software, and it is licensed under both the BSD-3 license
 * * and EPL-1.0 license. Full text of both licenses can be found in COPYING.BSD
 * * and COPYING.EPL files.
 * */

#include <limits.h>
#include <unistd.h>
#include <pthread.h>

#include <common/states.h>
#include <common/system/lock.h>
#include <common/types/event_type.h>
#include <common/database/db_helper.h>
#include <common/messaging/msg_internals.h>
#include <daemon/remote_api/eard_server_api.h>


#define BATSIM 1
#define MANUAL_MODE 0

#define IDLE_POWER 150
#define IDLE_JOB INT_MAX 

#define NEW_JOB 1
#define END_JOB 2

#define EXECUTE_JOB 1
#define JOB_COMPLETED 2


//maximum number of simulated jobs
#define MAX_SIM_JOBS 100000

typedef struct node{
	uint jobid;
	uint cur_power;
	uint req_power;
	uint powercap;
}node_t;

typedef enum {
    CPU,
    MEM,
    IO,
    MIX,
} app_type_t;

typedef struct jobs {
	uint jobid;
	int32_t is_active;
	int32_t *nodes;
	int32_t num_nodes;
	time_t start_time;
	time_t end_time;
    time_t last_time;
    int64_t energy;
    int64_t powercap_energy;
    int64_t req_power;
    app_type_t type;
} jobs_t;

typedef struct job_event {
	char event_type;
	int jid;
	int time;
} job_events_t;

typedef struct job_info {
	int num_cpus;
	int power;
} job_info_t;

typedef struct changes {
    int num_changes;
    int32_t* nodes;
    int32_t* pstates;
} pstate_changes_t;

typedef struct
{
    int flag;
    int index;
    int simulation; 
    double start_time;
    double end_time;
    char type[64];
    char id[16];
    int alloc;
} shmem_data_t;


void *remote_commands_manager(void *noarg);
void send_header_csv(int32_t fd);


#if MANUAL_MODE
void process_change_job_events(int type, int jobid);
void process_events();
#else
void process_change_job_events(int type, int jobid, time_t ttime);
void process_energy_jobs(jobs_t *job, time_t ttime);
void process_events(time_t ttime);
#endif

