
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#define MAX_POWER 400
#define MIN_POWER 300

#define NUM_APP_TYPES 4


static char *app_types[NUM_APP_TYPES] = {"CPU", "MEM", "IO", "MIX"};

void usage(char *app)
{
    printf("%s [num_jobs] [min power] [max power] [CPU perc] [MEM perc] [MIX perc]\n", app);
    printf("\t Percentages must be in floating point format (0.33 -> 33%%)\n");
    exit(1);
}

int32_t main(int32_t argc, char *argv[])
{
    if (argc < 7) usage(argv[0]);

    int32_t num_jobs = atoi(argv[1]);
    char outfile[128] = { 0 };
    sprintf(outfile, "powerusage.txt");
    int32_t min_power = atoi(argv[2]);
    int32_t max_power = atoi(argv[3]);
    float percs[3] = { 0 };
    percs[0] = atof(argv[4]);
    percs[1] = atof(argv[5]);
    percs[2] = atof(argv[6]);

    int32_t fd = open(outfile, O_WRONLY | O_CREAT | O_TRUNC , S_IRUSR|S_IWUSR|S_IRGRP);

    int32_t diff = max_power - min_power;
    for (int32_t job_id = 0; job_id < num_jobs; job_id ++)
    {
        int32_t power = rand()%diff + min_power;
        float perc_result = (float) (rand() % 100);

        int32_t app_type = 0;

        for (app_type = 0; app_type < sizeof(percs)/sizeof(float); app_type++) {
            if (perc_result < percs[app_type]*100) break;

            perc_result -= percs[app_type]*100;
        }
        dprintf(fd, "%d %d %s\n", job_id, power, app_types[app_type]);

    }

}
