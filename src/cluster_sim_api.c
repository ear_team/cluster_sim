/*
 * *
 * * This program is part of the EAR software.
 * *
 * * EAR provides a dynamic, transparent and ligth-weigth solution for
 * * Energy management. It has been developed in the context of the
 * * Barcelona Supercomputing Center (BSC)&Lenovo Collaboration project.
 * *
 * * Copyright © 2017-present BSC-Lenovo
 * * BSC Contact   mailto:ear-support@bsc.es
 * * Lenovo contact  mailto:hpchelp@lenovo.com
 * *
 * * EAR is an open source software, and it is licensed under both the BSD-3 license
 * * and EPL-1.0 license. Full text of both licenses can be found in COPYING.BSD
 * * and COPYING.EPL files.
 * */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#include <common/states.h>
#include <common/system/lock.h>
#include <common/types/event_type.h>
#include <common/database/db_helper.h>
#include <common/messaging/msg_internals.h>
#include <daemon/remote_api/eard_server_api.h>

#include <tools/cluster_sim_api.h>

db_conf_t db_conf;
extern int32_t stop_nodes;

#define ERROR_POWER 1500

extern pstate_changes_t changes;
extern node_t *cluster;
extern uint port;
extern uint cluster_size;
extern uint def_powercap;
extern uint idle_job;

extern jobs_t jobs[MAX_SIM_JOBS];
extern int current_jobs;
extern time_t original_time;
extern time_t current_time;

extern jobs_t csv_idle_job;
extern time_t csv_last_idle_time;

static powercap_status_t pc_status;
extern pthread_mutex_t cluster_lock;

static int host_ip = 300000;

extern int32_t event_file_fd;
extern int32_t csv_file_fd;
extern sem_t cluster_sim_sem;

int get_self_ip()
{
    int s;
    char buff[512];
    struct addrinfo hints;
    struct addrinfo *result, *rp;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM; /* STREAM socket */
    hints.ai_protocol = 0;          /* Any protocol */
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    gethostname(buff, 50);

    strtok(buff,".");

   	s = getaddrinfo(buff, NULL, &hints, &result);
    if (s != 0) {
		error("propagate_status:getaddrinfo fails for port %s (%s)",buff,strerror(errno));
		return EAR_ERROR;
    }

   	for (rp = result; rp != NULL; rp = rp->ai_next) {
        if (rp->ai_addr->sa_family == AF_INET)
        {
            struct sockaddr_in *saddr = (struct sockaddr_in*) (rp->ai_addr);

            return saddr->sin_addr.s_addr;
        }
    }
	return EAR_ERROR;
}

void fill_powercap_status()
{
	int n;
	uint crelease = 0;
	
	memset(&pc_status, 0, sizeof(powercap_status_t));
	if (state_fail(ear_trylock(&cluster_lock))) return ;

	pc_status.total_nodes = cluster_size;
	for (n = 0; n < cluster_size; n++){
		pc_status.total_powercap += cluster[n].powercap;
		pc_status.current_power += cluster[n].cur_power;
		pc_status.idle_nodes  += (cluster[n].jobid == IDLE_JOB);
		crelease    = (cluster[n].jobid == IDLE_JOB) && (IDLE_POWER < cluster[n].powercap);

		if (cluster[n].jobid == IDLE_JOB) {
			pc_status.total_idle_power += cluster[n].cur_power;
		}
		else {
			if (cluster[n].powercap < def_powercap) {
				pc_status.requested += def_powercap - cluster[n].powercap;
                cluster[n].powercap = def_powercap;
			}
			else if (cluster[n].req_power > cluster[n].powercap) {
				pc_status.num_greedy++;
				//set greedy ips
				pc_status.greedy_nodes = realloc(pc_status.greedy_nodes, pc_status.num_greedy * sizeof(int));
				pc_status.greedy_nodes[pc_status.num_greedy - 1] = (pc_status.num_greedy == 1) ? host_ip : n;

				//prepare greedy data
				pc_status.greedy_data = realloc(pc_status.greedy_data, pc_status.num_greedy * sizeof(greedy_bytes_t));
				memset(&pc_status.greedy_data[pc_status.num_greedy-1], 0, sizeof(greedy_bytes_t));

				pc_status.greedy_data[pc_status.num_greedy-1].requested = (cluster[n].req_power - cluster[n].powercap);
				pc_status.greedy_data[pc_status.num_greedy-1].stress = 
					100*(ear_max(1 - ((float)cluster[n].powercap / (float) cluster[n].req_power), 0));

				if (def_powercap < cluster[n].powercap) {
					pc_status.greedy_data[pc_status.num_greedy-1].extra_power = cluster[n].powercap - def_powercap;
				}
			} 
			else if (def_powercap < cluster[n].powercap) {
				pc_status.num_greedy++;
				//set greedy ips
				pc_status.greedy_nodes = realloc(pc_status.greedy_nodes, pc_status.num_greedy * sizeof(int));
				pc_status.greedy_nodes[pc_status.num_greedy - 1] = (pc_status.num_greedy == 1) ? host_ip : n;
				//prepare greedy data
				pc_status.greedy_data = realloc(pc_status.greedy_data, pc_status.num_greedy * sizeof(greedy_bytes_t));
				memset(&pc_status.greedy_data[pc_status.num_greedy-1], 0, sizeof(greedy_bytes_t));

				pc_status.greedy_data[pc_status.num_greedy-1].extra_power = cluster[n].powercap - def_powercap;
			}

		}

		if (crelease) pc_status.released += cluster[n].powercap - IDLE_POWER;
	}


	ear_unlock(&cluster_lock);
}


void generate_fake_event(int fd, int node_idx, time_t time, int event_type, int event_value)
{
	dprintf(fd, "2:%d:%d:%d:1:%lu:%d:%u\n", 
							//(node_idx+1)*2+1, idle_job, node_idx+1, time, event_type, event_value);
							node_idx*2+1, idle_job, node_idx+1, time, event_type, event_value);
}

void generate_event(int fd, int node_idx, int jobid, int task_id, time_t time, int event_type, int event_value)
{
	dprintf(fd, "2:%d:%d:%d:1:%lu:%d:%u\n", 
							(node_idx+1)*2, jobid, task_id, time, event_type, event_value);
							//(node_idx)*2, jobid, task_id, time, event_type, event_value);
                           
}

void send_event_csv(int32_t fd, int32_t num_nodes, time_t time, uint32_t job_id, uint32_t cur_power, uint32_t powercap, double stress, uint32_t req_power, uint32_t target_power) {
    if (job_id != IDLE_JOB) {
        dprintf(fd, "%ld;%d;%u;%u;%u;%.2lf;%u;%u;\n", 
                time, num_nodes, job_id, cur_power, powercap, stress, req_power, target_power);
    } else {
        dprintf(fd, "%ld;%d;%s;%u;%u;%.2lf;%u;%u;\n", 
                time, num_nodes, "NONE", cur_power, powercap, stress, req_power, target_power);
    }
}

void process_energy_jobs(jobs_t *job, time_t ttime)
{
    int64_t accum_energy = 0, powercap_energy = 0, req_power = 0;
    time_t diff_time = ttime - job->last_time;
    job->last_time = ttime;
    for (int i = 0; i < job->num_nodes; i++) {
        accum_energy += (int64_t)cluster[job->nodes[i]].cur_power * diff_time;
        powercap_energy += (int64_t)cluster[job->nodes[i]].powercap * diff_time;
        req_power += (int64_t)cluster[job->nodes[i]].req_power;
    }
    if (job->req_power == 0) job->req_power = req_power;
    job->energy += accum_energy;
    job->powercap_energy += powercap_energy;
}

void generate_events_job(jobs_t job, int value, time_t ttime) 
{
	int j;
	uint32_t powercap, cur_power, stress, req_power;
    uint32_t sum_powercap = 0, sum_cur_power = 0, sum_stress = 0, sum_req_power = 0, sum_target_power = 0;
	powercap = cur_power = stress = req_power = 0;
	for (j = 0; j < job.num_nodes; j++) {


		if (value) {
			powercap = cluster[job.nodes[j]].powercap;
			cur_power = cluster[job.nodes[j]].cur_power;
            if (powercap > 1000) printf("warning, powercap error: %u\n", powercap);
            if (cur_power > 1000) printf("warning, cur_power error: %u\n", cur_power);
			if (cluster[job.nodes[j]].req_power > cluster[job.nodes[j]].powercap) {
				stress = (uint) (100*(ear_max(1 - ((float)cluster[job.nodes[j]].powercap / (float) cluster[job.nodes[j]].req_power), 0)));
                if (stress > 100) {
                    printf("stress over 100%%, powercap %u and req_power %u\n", cluster[job.nodes[j]].powercap, cluster[job.nodes[j]].req_power);
                }
			}
			if (cluster[job.nodes[j]].req_power > cluster[job.nodes[j]].powercap) {
				req_power = cluster[job.nodes[j]].req_power - cluster[job.nodes[j]].powercap;
			}

		}
#if USE_PARAVER
		generate_event(event_file_fd, job.nodes[j], job.jobid, j+1, ttime-original_time, POWERCAP_VALUE, powercap);

		generate_event(event_file_fd, job.nodes[j], job.jobid, j+1, ttime-original_time, CURRENT_POWER, cur_power);

		generate_event(event_file_fd, job.nodes[j], job.jobid, j+1, ttime-original_time, CURRENT_STRESS, stress);

		generate_event(event_file_fd, job.nodes[j], job.jobid, j+1, ttime-original_time, REQ_POWER, req_power);

		generate_event(event_file_fd, job.nodes[j], job.jobid, j+1, ttime-original_time, TARGET_POWER, req_power + cur_power);

#endif
        sum_powercap += powercap;
        sum_cur_power += cur_power;
        sum_stress += stress;
        sum_req_power += req_power;
        sum_target_power += cluster[job.nodes[j]].req_power;
            /* CSV TRACE */
	}
    //send_event_csv(csv_file_fd, job.num_nodes, ttime, job.jobid, sum_cur_power, sum_powercap, (double)sum_stress/job.num_nodes, sum_req_power, sum_target_power);
}

void process_change_job_events(int type, int jobid, time_t ttime)
{
	int i;
	jobs_t job;
	if (state_fail(ear_trylock(&cluster_lock))) return;

	for (i = 0; i < current_jobs; i++) {
		if (jobs[i].jobid == jobid) {
			job = jobs[i];
			break;
		}
	}
	switch (type) 
	{
		case NEW_JOB:
			generate_events_job(job, 1, ttime); 
			break;
		case END_JOB:
			generate_events_job(job, 0, ttime); 
			break;
		default:
			printf("unknown event\n");
			break;
	}
	ear_unlock(&cluster_lock);

}

void send_header_csv(int32_t fd)
{
    dprintf(csv_file_fd, "%s;%s;%s;%s;%s;%s\n", "timestamp", "idle_power", 
            "num_idle_nodes", "active_power", "num_active_nodes", "active_stress");
#if 0
        dprintf(fd, "%s;%s;%s;%s;%s;%s;%s;%s;\n", 
                "timestamp", "num_nodes", "job_id", "cur_power", "powercap", "stress", "req_power", "target_power");
#endif
}

void process_events(time_t ttime)
{
	int i;
	if (state_fail(ear_trylock(&cluster_lock))) return;
    //printf("entering process_events with time %d\n", ttime-original_time);

    /* generate_fake_event functions are for paraver traces,
     * send_event_csv is for csv traces.
     * send_event_csv is also called inside generate_events_job for the events of
     * nodes with active jobs. We do not generate fake events for csv traces.*/
    int32_t idle_powercap = 0;
    int32_t idle_power    = 0;
    int32_t idle_nodes    = 0;
	for (i = 0; i < cluster_size; i++) {
		if (cluster[i].jobid != IDLE_JOB) {
#if USE_PARAVER
			//Current powercap
			generate_fake_event(event_file_fd, i, ttime-original_time, POWERCAP_VALUE, 0);

			//Current power
			generate_fake_event(event_file_fd, i, ttime-original_time, CURRENT_POWER, 0);

			//Current stress
			generate_fake_event(event_file_fd, i, ttime-original_time, CURRENT_STRESS, 0);


			//Requested power
			generate_fake_event(event_file_fd, i, ttime-original_time, REQ_POWER, 0);

			// Total power required
			generate_fake_event(event_file_fd, i, ttime-original_time, TARGET_POWER, 0);

		} else {
			generate_fake_event(event_file_fd, i, ttime-original_time, POWERCAP_VALUE, cluster[i].powercap);
			generate_fake_event(event_file_fd, i, ttime-original_time, CURRENT_POWER, cluster[i].cur_power);
			generate_fake_event(event_file_fd, i, ttime-original_time, CURRENT_STRESS, 0); //no need to calculate since node is idle
			generate_fake_event(event_file_fd, i, ttime-original_time, REQ_POWER, 0); //no need to calculate since node is idle
			generate_fake_event(event_file_fd, i, ttime-original_time, TARGET_POWER, cluster[i].cur_power); // Assuming cur_power = idle_power and it is guaranteed

#endif
            idle_powercap += cluster[i].powercap;
            idle_power    += cluster[i].cur_power;
            idle_nodes++;

		}

	}

    /* CSV TRACE */
    //send_event_csv(csv_file_fd, idle_nodes, ttime, 0, idle_power, idle_powercap, 0, 0, idle_power);
    for (i = 0; i < current_jobs; i++) {
        if (!jobs[i].is_active) continue;
        process_energy_jobs(&jobs[i], ttime);
        //generate_events_job(jobs[i], 1, ttime); 
    }
    int64_t csv_idle_power = 0;
    int64_t csv_active_power = 0;
    double  csv_active_stress = 0;
    int64_t csv_num_idle_nodes = 0;
    int64_t csv_num_active_nodes = 0;
    for (int i = 0; i < cluster_size; i++) {
        if (cluster[i].jobid != IDLE_JOB) {
            csv_active_power += cluster[i].cur_power;
            csv_active_stress += (double) (100*(ear_max(1 - ((double)cluster[i].powercap / (double) cluster[i].req_power), 0)));
            csv_num_active_nodes++;
        } else {
            csv_idle_power += cluster[i].cur_power;
            csv_num_idle_nodes++;
        }
    }
    csv_active_stress /= csv_num_active_nodes;
    dprintf(csv_file_fd, "%ld;%ld;%ld;%ld;%ld;%.2lf\n", ttime, csv_idle_power, csv_num_idle_nodes, csv_active_power, csv_num_active_nodes, csv_active_stress);

    time_t diff_time = ttime - csv_last_idle_time;
    for (i = 0; i < cluster_size; i++) {
        if (cluster[i].jobid == IDLE_JOB) {
            csv_idle_job.energy += cluster[i].cur_power * diff_time;
        }
    }

    csv_last_idle_time = ttime;

    ear_unlock(&cluster_lock);

}

void free_idle(pc_release_data_t *released) 
{
    int n;
    memset(released, 0, sizeof(pc_release_data_t));
    if (state_fail(ear_trylock(&cluster_lock))) return ;

    for (n = 0; n < cluster_size; n++) {
        if (cluster[n].jobid == IDLE_JOB) {
            if (cluster[n].req_power < (cluster[n].powercap * 0.75)) {
                released->released += cluster[n].powercap - IDLE_POWER;
                //cluster[n].powercap -= (cluster[n].powercap * 0.75) - cluster[n].req_power;
                cluster[n].powercap = IDLE_POWER;
                if (cluster[n].powercap < cluster[n].cur_power) cluster[n].cur_power = cluster[n].powercap;
            }
        }
    }
    ear_unlock(&cluster_lock);
}

#define NUM_PSTATES 6

//CPU
static float cpu_ref_power_steps[NUM_PSTATES] = {24.0, 22.0, 21.0, 20.0, 19.0, 0.01};
static uint32_t  cpu_ref_pstates[NUM_PSTATES] = {5, 4, 3, 2, 1, 0};

//MEM
static float mem_ref_power_steps[NUM_PSTATES] = {9.0, 8.0, 7.0, 6.0, 3.0, 0.01};
static uint32_t  mem_ref_pstates[NUM_PSTATES] = {10, 9, 8, 7, 6, 0};

//MIX
static float mix_ref_power_steps[NUM_PSTATES] = {58.0, 46.0, 40.0, 32.0, 26.0, 0.01};
static uint32_t  mix_ref_pstates[NUM_PSTATES] = {15, 14, 13, 12, 11, 0};

float prepare_pstate_change(int32_t change_idx, int32_t cluster_idx) 
{
    changes.nodes[change_idx] = cluster_idx;
    float curr_stress = 100*(1.0 - (float)cluster[cluster_idx].powercap/(float)cluster[cluster_idx].req_power);
    /* 
     * If there are two steps [22, 19] and a node has a stress of 20, we want to pick the step 22 for
     * the power/speed computation (since picking 19 would violate the requirement that the powercap is
     * enforced. Since the first step is always the lowest pstate, we can "ignore" that one for comparison,
     * since we only need to compare the one after.
     * This works by finding the first power_step that requires more power than we have currently allocated and
     * returning the previous one (which enforces the powercap)
     * */
    int32_t job_id = cluster[cluster_idx].jobid;
    app_type_t type = MIX;

    //search in reverse should be faster (the latest jobs are in the last part)
    for (int32_t i = current_jobs - 1; i >= 0; i--) { 
        if (jobs[i].jobid == job_id) {
            type = jobs[i].type;
            break;
        }
    }
    float *power_steps = NULL;
    uint32_t *ref_pstates = NULL;
    switch(type) {
        case CPU:
            power_steps = cpu_ref_power_steps;
            ref_pstates = cpu_ref_pstates;
            break;
        case MEM:
            power_steps = mem_ref_power_steps;
            ref_pstates = mem_ref_pstates;
            break;
        case MIX:
        default:
            power_steps = mix_ref_power_steps;
            ref_pstates = mix_ref_pstates;
            break;
    }
    for (int32_t i = 1; i < NUM_PSTATES; i++) {
        if (power_steps[i] < curr_stress) {
            changes.pstates[change_idx] = ref_pstates[i-1];
            return power_steps[i-1]/100.0;
        }
    }
    changes.pstates[change_idx] = 0;
    return 0.0;
}

void process_powercap_options(powercap_opt_t *options) 
{
    int32_t i, n;
    if (!options->num_greedy) return;
    if (state_fail(ear_trylock(&cluster_lock))) return;

    changes.num_changes = options->num_greedy;
    if (changes.num_changes == 0) {
        free(changes.pstates);
        free(changes.nodes);
    } else {
        changes.pstates = realloc(changes.pstates, sizeof(int32_t) * changes.num_changes);
        changes.nodes = realloc(changes.nodes, sizeof(int32_t) * changes.num_changes);
    }

    float change;
    for (n = 0; n < cluster_size; n++) {
        //the first node that has a job will be the one with hostip as the greedy_node
        if (cluster[n].jobid != IDLE_JOB && (cluster[n].powercap < cluster[n].req_power)) {
            cluster[n].powercap += options->extra_power[0];
            if (cluster[n].powercap > ERROR_POWER) {
                printf("warning, power set above the error threshold (%u)\n", cluster[n].powercap);
                printf("options were %d and powercap %u\n", options->extra_power[0], cluster[n].cur_power);
            }
            // changes in pstate
            change = prepare_pstate_change(0, n);
            // apply the power change
            cluster[n].cur_power = cluster[n].req_power*(1.0-change);
            //cluster[n].cur_power = cluster[n].powercap;
            //      printf("applying power change based on the pstate (%.2lf change, %u req_power %u final_power)\n",
            //          change, cluster[n].req_power, cluster[n].cur_power); 

            break;
        }
    }
    for (i = 1; i < options->num_greedy; i++) {
        //since the ip field contains the position on the list
        cluster[options->greedy_nodes[i]].powercap += options->extra_power[i];
        if (cluster[options->greedy_nodes[i]].powercap > ERROR_POWER) {
            printf("warning, power set above the error threshold (%u)\n", cluster[options->greedy_nodes[i]].powercap);
            printf("options were %d and powercap %u\n", options->extra_power[i], cluster[options->greedy_nodes[i]].cur_power);
        }
        change = prepare_pstate_change(i, options->greedy_nodes[i]);
        // apply the power change
        cluster[options->greedy_nodes[i]].cur_power = cluster[options->greedy_nodes[i]].req_power*(1.0-change);
        //cluster[options->greedy_nodes[i]].cur_power = cluster[options->greedy_nodes[i]].powercap;
        //    printf("applying power change based on the pstate (%.2lf change, %u req_power %u final_power)\n",
        //           change, cluster[options->greedy_nodes[i]].req_power, cluster[options->greedy_nodes[i]].cur_power); 
    }

    //cleanup: all ask_default nodes will set themselves to that value
    for (n = 0; n < cluster_size; n++) {
        if (cluster[n].jobid == IDLE_JOB) continue;
        if (cluster[n].powercap < def_powercap) {
            cluster[n].powercap = def_powercap;
            cluster[n].cur_power = def_powercap;
        }
    }

    ear_unlock(&cluster_lock);
}

power_check_t get_mon_power()
{
    uint64_t total_power = 0;
    int64_t num_nodes = 0;

    changes.num_changes = 0;
    if (changes.pstates != NULL) {
        free(changes.pstates);
        free(changes.nodes);
        changes.pstates = NULL;
        changes.nodes= NULL;
    }
    for (int32_t i = 0; i < cluster_size; i++) {
        if (cluster[i].jobid != IDLE_JOB) {
            changes.num_changes++;
            changes.nodes = realloc(changes.nodes, sizeof(int32_t) * changes.num_changes);
            changes.pstates = realloc(changes.pstates, sizeof(int32_t) * changes.num_changes);
            float change = prepare_pstate_change(changes.num_changes-1, i);
            cluster[i].cur_power = cluster[i].req_power*(1.0-change);
        }
        total_power += cluster[i].cur_power;
        num_nodes++;
    }

    //printf("get_mon_power has set %d changes\n", changes.num_changes);

    return (power_check_t) { total_power, num_nodes };
}

void process_req(int fd)
{
    request_t command;
    pc_release_data_t released;
    long ack = EAR_SUCCESS;
    int req;
    char *status_data;
    power_check_t power = { 0 };

    memset(&command, 0, sizeof(request_t));
    command.req = NO_COMMAND;

    req = read_command(fd, &command);
    if (req == EAR_SOCK_DISCONNECTED) return;

    send_answer(fd, &ack); //no need to propagate
    switch (req) {
        case EAR_RC_GET_POWERCAP_STATUS:
            fill_powercap_status();
            status_data = mem_alloc_char_powercap_status(&pc_status);
            send_data(fd, sizeof(powercap_status_t) + ((sizeof(greedy_bytes_t) + sizeof(int))*pc_status.num_greedy), 
                    status_data, EAR_TYPE_POWER_STATUS);
            if (pc_status.num_greedy > 0) {
                free(pc_status.greedy_nodes);
                free(pc_status.greedy_data);
            }
            free(status_data);
            break;
        case EAR_RC_SET_POWERCAP_OPT:
            process_powercap_options(&command.my_req.pc_opt);
            free(command.my_req.pc_opt.greedy_nodes);
            free(command.my_req.pc_opt.extra_power);
            process_events(current_time);
            sem_post(&cluster_sim_sem);
            break;
        case EAR_RC_GET_POWER:
            power = get_mon_power();
            send_data(fd, sizeof(power_check_t), (char *)&power, EAR_TYPE_POWER_CHECK);
            process_events(current_time);
            sem_post(&cluster_sim_sem);
            break;
        case EAR_RC_RELEASE_IDLE:
            free_idle(&released);
            send_data(fd, sizeof(pc_release_data_t), (char *)&released, EAR_TYPE_RELEASED);
            break;
        case EARGM_STOP_NODES:
            ear_lock(&cluster_lock);
            stop_nodes = 1;
            ear_unlock(&cluster_lock);
            break;
        case EARGM_START_NODES:
            ear_lock(&cluster_lock);
            stop_nodes = 0;
            ear_unlock(&cluster_lock);
            break;
        default:
            //printf("Received command %d\n", req);
            break;
    }

}

void *remote_commands_manager(void *noarg)
{
    int remote_client_fd;
    struct sockaddr_in remote_client;

    //printf("starting remote commands thread\n");

    int server_socket = create_server_socket(port);
    if (server_socket < 0) {
        printf("ERROR creating server socket\n");
        pthread_exit(0);
    }

    host_ip = get_self_ip();

    memset(&db_conf, 0, sizeof(db_conf_t));

    while(1){
        //printf("waiting for client\n");
        remote_client_fd = wait_for_client(server_socket, &remote_client);
        if (remote_client_fd < 0) {
            printf("ERROR wait_for_client fails\n");
            pthread_exit(0);
        } else {
            //printf("entering request processing\n");

            process_req(remote_client_fd);
            close(remote_client_fd);
        }
    }

    close_server_socket(server_socket);
    pthread_exit(0);
}
