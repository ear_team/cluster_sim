/*
 * *
 * * This program is part of the EAR software.
 * *
 * * EAR provides a dynamic, transparent and ligth-weigth solution for
 * * Energy management. It has been developed in the context of the
 * * Barcelona Supercomputing Center (BSC)&Lenovo Collaboration project.
 * *
 * * Copyright © 2017-present BSC-Lenovo
 * * BSC Contact   mailto:ear-support@bsc.es
 * * Lenovo contact  mailto:hpchelp@lenovo.com
 * *
 * * EAR is an open source software, and it is licensed under both the BSD-3 license
 * * and EPL-1.0 license. Full text of both licenses can be found in COPYING.BSD
 * * and COPYING.EPL files.
 * */

#include <time.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <pthread.h>
#include <string.h>
#include <semaphore.h>
#include <errno.h>

#include <sys/fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <common/states.h>
#include <common/system/lock.h>
#include <common/types/generic.h>

#include <tools/cluster_sim_api.h>

#include <global_manager/eargm_rapi.h>


// manual mode -> user inputs jobs
// non-manual -> jobs come from a file

/* cluster_sim num_nodes min_idle max_power port */
#define MIN_NUM_ARGS 1
#define EXIT_OPT 5

/* Whether EARGM responds at every new job or every X seconds. RESCHEDULE_TIME is the amount of seconds it takes between
 * EARGM cycles. */
#define DISABLED_RESCHED 0
#define NAIVE_RESCHED 1
#define REALISTIC_RESCHED 2
#define EARGM_RESCHED_TYPE NAIVE_RESCHED
#define RESCHEDULE_TIME 30

bool verbose = false;

static pthread_t remote_commands_manager_th;

sem_t cluster_sim_sem;

typedef struct power_usage {
    int32_t job_id;
    int32_t req_power;
    char app_type[16];
} power_usage_t;

static int num_p_usages = 0;
power_usage_t p_usage[MAX_SIM_JOBS];
jobs_t jobs[MAX_SIM_JOBS];
int current_jobs = 0;

jobs_t csv_idle_job = { 0 };
time_t csv_last_idle_time = 0;

job_info_t *j_info = NULL;
job_events_t *j_evs = NULL;
int num_events = 0;
int num_jobs = 0;

node_t *cluster;
pstate_changes_t changes = { 0 };

pthread_mutex_t cluster_lock = PTHREAD_MUTEX_INITIALIZER;
uint idlepower, cluster_size, port, def_powercap, idle_job;
uint used_nodes = 0;
uint max_sim_jobs = MAX_SIM_JOBS;

static int header_file_fd = -1;
int32_t event_file_fd = -1;
int32_t csv_file_fd = -1;
static int csv_summary_file_fd = -1;
static int powerusage_fd = -1;
time_t original_time = 0;
time_t max_time = 0;
time_t prev_time = 0;
time_t current_time = 0;

char csv_tag[64] = "NONE";

int32_t stop_nodes = 0;

void usage(char *name)
{
	printf("%s [no parameters]\n", name);
	exit(1);
}

int cs_to_bs_pipe_fd = -1;
int bs_to_cs_pipe_fd = -1;

void init_nodes(uint idle, uint def)
{
	for (uint n = 0; n < cluster_size; n++){
		cluster[n].powercap  = def;
		cluster[n].cur_power = idle;
		cluster[n].jobid = IDLE_JOB;
	}
}

int compare_jobs(const void *a, const void *b)
{
    jobs_t *j1 = (jobs_t *)a;
    jobs_t *j2 = (jobs_t *)b;
    if (j1->jobid > j2->jobid) return 1;
    else if (j1->jobid < j2->jobid) return -1;
    else return 0;
}

void finish_jobs()
{
	if (max_time == 0) max_time = time(NULL);
	int i, j;
    char *node_cpus = calloc(cluster_size*10, sizeof(char));
	struct tm timeinfo = *localtime(&original_time);


    qsort(jobs, current_jobs, sizeof(jobs_t), compare_jobs);
#if USE_PARAVER
	//HEADER
	sprintf(node_cpus, "%d(", cluster_size);
	for (i = 0; i < cluster_size-1; i++) {
		strcat(node_cpus, "2,"); //1 cpu per node
	}
	strcat(node_cpus, "2)"); //last one closes parentheses
	dprintf(header_file_fd, "#Paraver (%d/%d/%d at %d:%d):%ld:%s:%d", 
			timeinfo.tm_mday, timeinfo.tm_mon, timeinfo.tm_year+1900, timeinfo.tm_hour, timeinfo.tm_min,
			max_time-original_time, node_cpus, current_jobs+1); //current_jobs+1 because we have the "fake" job
                                                                

	for (i = 0; i < current_jobs; i++)
	{
		dprintf(header_file_fd, ":%d(", jobs[i].num_nodes);
		for (j = 0; j < jobs[i].num_nodes-1; j++) {
			dprintf(header_file_fd, "1:%d,", jobs[i].nodes[j]+1);
		}
		dprintf(header_file_fd, "1:%d)", jobs[i].nodes[jobs[i].num_nodes-1]+1);
	}

	dprintf(header_file_fd, ":%d(", cluster_size); //fake job
	for (i = 0; i < cluster_size-1; i++)
	{
		dprintf(header_file_fd, "1:%d,", i+1);
	}
	dprintf(header_file_fd, "1:%d)", cluster_size);
	dprintf(header_file_fd, "\n");
	

	// STATE logs
	for (i = 0; i < current_jobs; i++)
	{
		if (jobs[i].end_time == 0) jobs[i].end_time = max_time;

		for (j = 0; j < jobs[i].num_nodes; j++) {
			dprintf(header_file_fd, "1:%d:%d:%d:1:%lu:%lu:1\n", (jobs[i].nodes[j]+1)*2, jobs[i].jobid, j+1, jobs[i].start_time, jobs[i].end_time);
		}
	}

#endif
	for (i = 0; i < current_jobs; i++)
	{
        printf("Job with id %d, num_nodes %d, start_time %lu and end_time %lu\n", jobs[i].jobid, jobs[i].num_nodes,
                                                                                jobs[i].start_time, jobs[i].end_time);
    }
    time_t diff_time = max_time - csv_last_idle_time;
    for (i = 0; i < cluster_size; i++) {
        if (cluster[i].jobid == IDLE_JOB) {
            csv_idle_job.energy += cluster[i].cur_power * diff_time;
        }
    }
    dprintf(csv_summary_file_fd, "%s;%s;%ld;%ld;%.2lf;%ld;%u;%ld;%d;\n", csv_tag, "IDLE", csv_idle_job.start_time, 
            max_time, (double)csv_idle_job.energy/(max_time-csv_idle_job.start_time), 0L, 
                    idlepower, csv_idle_job.energy, csv_idle_job.num_nodes);
    csv_last_idle_time = max_time;
    free(node_cpus);
}

int cluster_sim_new_job(uint jobid, uint reqnodes, int32_t *nodes, uint reqpower, app_type_t type, time_t ttime)
{
    if (state_fail(ear_trylock(&cluster_lock))) return 0;
	int save_job = 0; //can we store the job? also works as an index
	
	if ((used_nodes + reqnodes) > cluster_size){
		ear_unlock(&cluster_lock);
		printf("New job does not have enough number of free nodes \n");
		return 0;
	}
	if (current_jobs < MAX_SIM_JOBS) {
		save_job = 1; //we need to store the nodes
		memset(&jobs[current_jobs], 0, sizeof(jobs_t));
		jobs[current_jobs].nodes = calloc(reqnodes, sizeof(int));
		jobs[current_jobs].num_nodes = reqnodes;
		jobs[current_jobs].jobid = jobid;
		jobs[current_jobs].is_active = 1;
		jobs[current_jobs].start_time = ttime;
		jobs[current_jobs].last_time = ttime;
		jobs[current_jobs].energy = 0;
		jobs[current_jobs].powercap_energy = 0;
        jobs[current_jobs].type = type;
        if (ttime > current_time) {
            current_time = ttime;
        } else {
            current_time++;
        }
	} else {
		printf("Simulated maximum number of jobs, this job will not be saved \n");
	}

    used_nodes += reqnodes;

    uint32_t i;
    for (i = 0; i < reqnodes; i++) {
        if (nodes[i] >= (int32_t)cluster_size) {
            printf("Invalid node id %d\n", nodes[i]);
            return 0;
            ear_unlock(&cluster_lock);
        }
        if (cluster[nodes[i]].jobid != IDLE_JOB) {
            printf("Invalid node id %d (has jobid %d)\n", nodes[i], cluster[nodes[i]].jobid);
            return 0;
            ear_unlock(&cluster_lock);
        }
        cluster[nodes[i]].jobid = jobid;
        cluster[nodes[i]].cur_power = ear_min(cluster[nodes[i]].powercap, reqpower);
        cluster[nodes[i]].req_power = reqpower;
        if (save_job) {
            jobs[current_jobs].nodes[save_job-1] = nodes[i];
            save_job++;
        }
    }

    process_energy_jobs(&jobs[current_jobs], ttime);
    if (save_job) current_jobs++;
    //printf("current_jobs: %d\n", current_jobs);

    ear_unlock(&cluster_lock);
    return 1;

}

void cluster_sim_end_job(uint jobid, time_t ttime)
{
    if (state_fail(ear_trylock(&cluster_lock))) return ;

    if (ttime > current_time) {
        current_time = ttime;
    } else {
        current_time++;
    }

    int i;
    for (i = 0; i < current_jobs; i++) {
        if (jobs[i].jobid == jobid) {
            jobs[i].end_time = ttime;
            jobs[i].is_active = 0;
            break;
        }
    }
    if (i < current_jobs) {
        process_energy_jobs(&jobs[i], ttime);
        if (jobs[i].start_time == jobs[i].end_time) {
            dprintf(csv_summary_file_fd, "%s;%u;%ld;%ld;%.2lf;%ld;%ld;%ld;%d;\n", csv_tag, jobid, jobs[i].start_time, jobs[i].end_time, 
                    0.0, 0L, jobs[i].req_power, jobs[i].energy, jobs[i].num_nodes);
        }
        else {
            dprintf(csv_summary_file_fd, "%s;%u;%ld;%ld;%.2lf;%ld;%ld;%ld;%d;\n", csv_tag, jobid, jobs[i].start_time, jobs[i].end_time, 
                    (double)jobs[i].energy/(jobs[i].end_time-jobs[i].start_time), jobs[i].powercap_energy/(jobs[i].end_time-jobs[i].start_time), 
                    jobs[i].req_power, jobs[i].energy, jobs[i].num_nodes);
        }
    }
    //we do this at the end so that the idlepower does not affect the proces_energy_jobs
    for (int n = 0; n < cluster_size; n++){
        if (cluster[n].jobid == jobid){
            used_nodes --;
            cluster[n].jobid = IDLE_JOB;
            cluster[n].cur_power = idlepower;	
            cluster[n].req_power = 0;
        }
    }
    printf("Finished job %d with time %ld (runtime %ld)\n", jobid, ttime, jobs[i].end_time - jobs[i].start_time);
    ear_unlock(&cluster_lock);
}

void summary_csv_print_header()
{
    dprintf(csv_summary_file_fd, "%s;%s;%s;%s;%s;%s;%s;%s;%s;\n", "tag", "job_id", "start_time", "end_time", 
            "average power", "average powercap", "requested power", "energy", "num_nodes");
}


void print_job(uint i)
{
    printf("NODE[%d] ID %u power %u req power %u W allocated power %u W stress %f\n", i, cluster[i].jobid, cluster[i].cur_power, cluster[i].req_power, 
            cluster[i].powercap, (cluster[i].req_power > cluster[i].powercap)? 1.0 - ((float)cluster[i].powercap) / (float)cluster[i].req_power: 0.0);
}

void print_jobs()
{
    if (state_fail(ear_trylock(&cluster_lock))) return ;
    for (uint n = 0; n < cluster_size; n++){
        if (cluster[n].jobid) print_job(n);
    }
    ear_unlock(&cluster_lock);
}

void print_status()
{
    uint total_power = 0;
    uint idle_nodes  = 0;
    uint allocated   = 0;
    uint cgreedy, greedy      = 0;
    uint crelease,release     = 0;
    uint total_extra = 0;
    uint total_release = 0;

    if (state_fail(ear_trylock(&cluster_lock))) return ;

    for (uint n = 0; n < cluster_size; n++){
        allocated   += cluster[n].powercap;
        total_power += cluster[n].cur_power;
        idle_nodes  += (cluster[n].jobid == IDLE_JOB);
        cgreedy     = (cluster[n].req_power > cluster[n].powercap);
        crelease    = ((cluster[n].jobid > 0) && (cluster[n].req_power < cluster[n].powercap * 0.75));
        greedy      += cgreedy;
        release     += crelease;
        if (cgreedy) total_extra += cluster[n].req_power - cluster[n].powercap;
        if (crelease)total_release += (cluster[n].powercap * 0.75) - cluster[n].req_power;
    }

    printf("Cluster status: allocated power %u W, current power %u W,  idle nodes %u, greedy nodes %u, release nodes %u\n", 
            allocated, total_power, idle_nodes, greedy, release);
    printf("total extra power requested %u W, total power to be release %u W\n", total_extra, total_release); 

    ear_unlock(&cluster_lock);
}

void send_pstate_changes()
{
    write(cs_to_bs_pipe_fd, &changes.num_changes, sizeof(int));

    if (changes.num_changes == 0) {
        return;
    }

    for (int i = 0; i < changes.num_changes; i++) {
        write(cs_to_bs_pipe_fd, &changes.nodes[i], sizeof(int32_t));
    }

    for (int i = 0; i < changes.num_changes; i++) {
        write(cs_to_bs_pipe_fd, &changes.pstates[i], sizeof(int32_t));
    }

}

void read_power_usage()
{
    if (powerusage_fd < 0) {
        printf("read_power_usage called with non-valid fd\n");
        exit(0);
    }
    FILE *tmp = fdopen(powerusage_fd, "r");
    char *line = NULL;
    size_t len = 0;
    while ((getline(&line, &len, tmp) != -1) && num_p_usages < MAX_SIM_JOBS) {
        sscanf(line, "%d %d %s", 
                &p_usage[num_p_usages].job_id, 
                &p_usage[num_p_usages].req_power, 
                p_usage[num_p_usages].app_type);
#if 0
        printf("read power usage for job_id %d, power %d and app_type %s\n",
                p_usage[num_p_usages].job_id, 
                p_usage[num_p_usages].req_power, 
                p_usage[num_p_usages].app_type);
#endif

        num_p_usages++;
    }
    if (!(num_p_usages < MAX_SIM_JOBS)) {
        printf("Warning, there are more power usage entries than MAX_SIM_JOBS\n");
    }
}

struct pair {
    int32_t power;
    app_type_t type;
};

app_type_t str_to_apptype(const char *t)
{
    if (!strcmp(t, "CPU")) {
        return CPU;
    }
    if (!strcmp(t, "MEM")) {
        return MEM;
    }
    if (!strcmp(t, "IO")) {
        return IO;
    }
    return MIX;
}

struct pair get_powerusage(int32_t job_id)
{
    for (int32_t i = 0; i < num_p_usages; i++) {
        if (p_usage[i].job_id == job_id) return (struct pair) { p_usage[i].req_power, str_to_apptype(p_usage[i].app_type)};
    }
    return (struct pair){0};
}

int main(int argc,char *argv[])
{

    if (argc < MIN_NUM_ARGS) usage(argv[0]);

    if (argc > 1) strncpy(csv_tag, argv[1], 64);

    int ret;

    port = 50001; //careful with the port, in manual mode it needs to be set before starting the remote thread

    shmem_data_t* sdata = NULL;

    int fd = -1;

    int tmp_ret = 0;

    char powerusage_filename[64] = { 0 };

    int send_stop_nodes = 1;
    original_time = time(NULL);
    idlepower = 150;
    def_powercap = 200;
    int power = 250;
    cluster_size = 512;


    char *def_powercap_env = getenv("CLUSTER_SIM_DEF_POWERCAP");
    if (def_powercap_env) {
        def_powercap = atoi(def_powercap_env);
        printf("CLUSTER_SIM_DEF_POWERCAP has been read; powercap value %u\n", def_powercap);
    } else {
        printf("WARNING: CLUSTER_SIM_DEF_POWERCAP not set; using default powercap value %u\n", def_powercap);
        char line[1024] = { 0 };
        fprintf(stderr, "Do you want to continue? [y/N]: ");
        while (fgets(line, sizeof(line), stdin)) {
            if (!strncasecmp(line, "y", 1)) { 
                break; 
            } else if (line[0] == '\n' || !strncasecmp(line, "n", 1)) {
                exit(1);
            }
            fprintf(stderr, "Do you want to continue? [y/N]: ");
        }
    }
    char *def_cluster_num_nodes = getenv("CLUSTER_SIM_NUM_NODES");
    if (def_cluster_num_nodes) {
        cluster_size = atoi(def_cluster_num_nodes);
        printf("CLUSTER_SIM_NUM_NODES has been read; current cluster size %u\n", cluster_size);
    } else {
        printf("WARNING: CLUSTER_SIM_NUM_NODES not set; using default num_nodes %u\n", cluster_size);
        char line[1024] = { 0 };
        fprintf(stderr, "Do you want to continue? [y/N]: ");
        while (fgets(line, sizeof(line), stdin)) {
            if (!strncasecmp(line, "y", 1)) { 
                break; 
            } else if (line[0] == '\n' || !strncasecmp(line, "n", 1)) {
                exit(1);
            }
            fprintf(stderr, "Do you want to continue? [y/N]: ");
        }
    }
    char *send_stop_nodes_env = getenv("CLUSTER_SIM_STOP_NODES");
    if (send_stop_nodes_env) {
        send_stop_nodes = atoi(send_stop_nodes_env);
        printf("CLUSTER_SIM_STOP_NODES has been set to %u\n", send_stop_nodes);
    }

    if (argc > 2) strncpy(powerusage_filename, argv[2], 64);
    powerusage_fd = open(powerusage_filename, O_RDONLY);

    if (powerusage_fd >= 0) {
        read_power_usage();
    }

    if ((ret=pthread_create(&remote_commands_manager_th, NULL, remote_commands_manager, NULL))){
        printf("Error creating remote_commands_manager thread\n");
        exit(1);
    }
    sem_init(&cluster_sim_sem, 0, 0);


    //init
    cluster = calloc(cluster_size, sizeof(node_t));
    init_nodes(idlepower, def_powercap);

    // Write pipe
    if ((tmp_ret = mkfifo("/tmp/cluster_sim_pipe", S_IRUSR | S_IRGRP | S_IWUSR | S_IWGRP | S_IROTH | S_IWOTH)) < 0) {
        if (errno == EEXIST) printf("cluster_sim_pipe already exists, continuing\n");
        else {
            printf("cluster_sim_pipe could not be created (%s), exiting\n", strerror(errno));
            exit(1);
        }
    } else {
        printf("cluster_sim_pipe created successfully!\n");
    }
    cs_to_bs_pipe_fd = open("/tmp/cluster_sim_pipe", O_WRONLY); 
    if (cs_to_bs_pipe_fd > 1) printf("pipe succesfuly opened!\n");


    // Read pipe
    if ((tmp_ret = mkfifo("/tmp/batsched_pipe", S_IRUSR | S_IRGRP | S_IWUSR | S_IWGRP | S_IROTH | S_IWOTH)) < 0) {
        if (errno == EEXIST) printf("batsched_pipe already exists, continuing\n");
        else {
            printf("batsched_pipe could not be created (%s), exiting\n", strerror(errno));
            exit(1);
        }
    } else {
        printf("batsched_pipe created successfully!\n");
    }
    bs_to_cs_pipe_fd = open("/tmp/batsched_pipe", O_RDONLY); 
    if (bs_to_cs_pipe_fd > 1) printf("pipe succesfuly opened!\n");


    // Shared memory
    fd = shm_open("/ear.dat", O_RDWR, 1);
    if (fd == -1) {
        printf("Error opening shmem object\n");
        exit(1);
    }

    sdata = (shmem_data_t*) mmap(0, sizeof(shmem_data_t), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

    if (sdata == (shmem_data_t*) -1 || sdata == NULL) {
        printf("Error mapping shared memory\n");
        exit(1);
    }

    sdata->index = 1;

    //tell batsched that we are ready to read
    int start = 1;
    write(cs_to_bs_pipe_fd, &start, sizeof(int));

    //and read the first to sync
    read(bs_to_cs_pipe_fd, &start, sizeof(int));

    printf("Finished initial sync with batsim\n");

    printf("Opening trace files...\n");
#if USE_PARAVER
    header_file_fd = open("paraver_header.txt", O_WRONLY | O_CREAT | O_TRUNC, 
            S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    event_file_fd = open("paraver_events.txt", O_WRONLY | O_CREAT | O_TRUNC, 
            S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
#endif

    char tmp[64] = { 0 };
    time_t tmp_time = time(NULL);
    sprintf(tmp, "trace%ld.csv", tmp_time);
    csv_file_fd = open(tmp, O_WRONLY | O_CREAT | O_TRUNC, 
            S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);

    sprintf(tmp, "trace%ldsummary.csv", tmp_time);
    csv_summary_file_fd = open(tmp, O_WRONLY | O_CREAT | O_TRUNC, 
            S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    send_header_csv(csv_file_fd);
    summary_csv_print_header();
    printf("Finished opening trace files\n");


    while (sdata->simulation != 0)
    {
        //waiting for batsim to message us
        if (verbose) printf("Waiting for batsim to message\n");
        read(bs_to_cs_pipe_fd, &start, sizeof(int));
        if (verbose) printf("Received new message from batsim\n");

        start = 0;

        bool done_something = false;
        //NEW JOB event
        if (strcmp(sdata->type, "EXECUTE_JOB") == 0 && sdata->start_time >= 0 && atoi(&sdata->id[3]) >= 0 && sdata->alloc > 0) {

            done_something = true;
            if (verbose) printf("starting to read nodes for job %c\n", sdata->id[3]);
            int *tmp = calloc(sdata->alloc, sizeof(int));
            for (int i = 0; i < sdata->alloc; i++) {
                read(bs_to_cs_pipe_fd, &tmp[i], sizeof(int));
                if (verbose) printf("read node from job %c, nodeid %d\n", sdata->id[3], tmp[i]);
            }

            send_pstate_changes();
            if (powerusage_fd >= 0) {
                struct pair p = get_powerusage(atoi(&sdata->id[3]));
                cluster_sim_new_job(atoi(&sdata->id[3]), sdata->alloc, tmp, p.power, p.type, sdata->start_time);
            }
            else {
                cluster_sim_new_job(atoi(&sdata->id[3]), sdata->alloc, tmp, power, MIX, sdata->start_time);
            }
            free(tmp);
            max_time = sdata->start_time;
            //printf("Created new job with id %d\n", atoi(&sdata->id[3]));
        }

        //END JOB event
        else if (strcmp(sdata->type, "JOB_COMPLETED") == 0 && sdata->end_time > 0 && atoi(&sdata->id[3]) >= 0) {
            done_something = true;
            cluster_sim_end_job(atoi(&sdata->id[3]), sdata->end_time);
            max_time = sdata->end_time;
            //printf("Finished job with id %d\n", atoi(&sdata->id[3]));
        }

        if (!done_something) {
            printf("WARNING: gotten an event from batsched that is being ignored. type %s\n", sdata->type);
            ear_lock(&cluster_lock);
            write(cs_to_bs_pipe_fd, &stop_nodes, sizeof(int));
            ear_unlock(&cluster_lock);
            continue;
        }

        if (sdata->simulation != 0) {
#if EARGM_RESCHED_TYPE == NAIVE_RESCHED
            if (max_time != prev_time || max_time >= (prev_time + RESCHEDULE_TIME)) {
                prev_time = max_time;
                eargm_reschedule_single("void", 50100);
                sem_wait(&cluster_sim_sem);
            }
#elif !EARGM_RESCHED_TYPE
            eargm_reschedule_single("void", 50100);
            sem_wait(&cluster_sim_sem);
#endif
        }

        sdata->start_time = 0.0;
        sdata->end_time = 0.0;
        sdata->type[0] = '\0';
        sdata->id[0] = '\0';
        sdata->alloc = 0;

        //notify that we have read the data
        if (verbose) printf("notifying batsim that we have finished with stop_nodes %d\n", stop_nodes);

        ear_lock(&cluster_lock);
        stop_nodes &= send_stop_nodes;
        if (stop_nodes) printf("nodes have been stopped!\n");
        write(cs_to_bs_pipe_fd, &stop_nodes, sizeof(int));
        ear_unlock(&cluster_lock);
    }
    munmap(sdata, sizeof(shmem_data_t));
    close(fd);

    int test = 1;
    printf("waiting for batsim to message\n");
    read(bs_to_cs_pipe_fd, &test, sizeof(int));
    printf("Read test %d\n", test);
    close(cs_to_bs_pipe_fd);
    close(bs_to_cs_pipe_fd);

    finish_jobs();
    printf("Simulation End\n");

#if USE_PARAVER
    system("cp paraver_header.txt sim_out.prv");
    system("cat paraver_events.txt >> sim_out.prv");
#endif

    return 0;
}
