#!/bin/python3

import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tabulate import tabulate

# Get a list of the trace swf and the csv files
print("\n")

if len(sys.argv) < 3: 
    print("usage is cluster_sim_plot.py [SWF file path] [baseline.csv,other.csv,...]")
    exit()

#file9 = input("Enter the swf trace file name was used: ")
file9 = sys.argv[1]
df9 = pd.read_csv(file9, header=None, sep='\s+')
timeFactor = df9.iloc[0, 1]   # get the submit_time of the fisrt job
df9.rename(columns={0: 'job_id'}, inplace=True)
df9.rename(columns={1: 'submit_time'}, inplace=True)

'''
new_record = pd.DataFrame({
    0: [value1],  # Assuming the first column is at index 0
    1: [value2],  # Assuming the second column is at index 1
    # Add more columns and corresponding values as needed
})

# Append the new record
updated_df = existing_df.append(new_record, ignore_index=True)
'''

#print(df9.head(10))
#print("\n")

#file_names_input = input("Enter the file names separated by comma (e.g., file1.csv, file2.csv, file3.csv): ")
file_names_input = sys.argv[2]
print("\n")

# Split the input string into individual file names
file_names = file_names_input.split(',')

# Load each file separately into a DataFrame
dfs = []
file_names_list = []
for file_name in file_names:
    data = pd.read_csv(file_name.strip(), sep=';')
    
    data['job_id'] = data['job_id'].replace('IDLE', np.nan)
    data['job_id'] = pd.to_numeric(data['job_id'], errors='coerce').astype(float)
    data['job_id'] = data['job_id'].fillna(999999).astype(int)
    
    df = data.sort_values(by='job_id')
    dfs.append(df)
    
    name = file_name.split('.')[0].strip()
    file_names_list.append(name)
    
    #print(df.head(10))
    #print(df.tail(10))
    #print("\n")


# Transforming start_time & end_time and calculate the other times:
for i, df in enumerate(dfs, start=1):
    new_name = f"df{i}"
    globals()[new_name] = df
    
    
    df['job_id'] = df['job_id'] + 1
    df['start_time'] = df['start_time'] + timeFactor
    df['end_time'] = df['end_time'] + timeFactor
    df.insert(4, 'run_time', df['end_time'] - df['start_time'])
    
    #print(df.dtypes)
    #print(df.head(10))
    #print(df9.head(10))
    
    merged_df = pd.merge(df, df9, on='job_id')
    
    #print(merged_df.columns)
    #print(merged_df[['job_id', 'start_time', 'end_time', 'submit_time']].head(10))
    #print("\n")
    
    merged_df.insert(5, 'wait_time', merged_df['start_time'] - merged_df['submit_time'])
    merged_df.insert(6, 'response_time', merged_df['end_time'] - merged_df['submit_time'])
    
    #print(merged_df.tail(10))
    
    merged_df.set_index('job_id', inplace=True)
    df['wait_time'] = df['job_id'].map(merged_df['wait_time'])
    df['response_time'] = df['job_id'].map(merged_df['response_time'])
    
    #print(merged_df.head(10))
    #print("\n")
    
    #print(df.head(10))
    #print(df.tail(10))
    #print("\n")
    
    del merged_df


# Calculate the run_time_percentage_increase & wait_time_percentage_increase:
columns_to_merge = ['job_id', 'run_time', 'wait_time', 'response_time', 'energy']
baseline_df = dfs[0][columns_to_merge].copy()

energy_consumed_list = []
run_time_percentage_increase_list = []
wait_time_percentage_increase_list = []
response_time_percentage_increase_list = []
energy_consumed_percentage_increase_list = []
wait_time_increase_list = []

for df in dfs[1:]:
    df.reset_index(inplace=True)
    merged_df = pd.merge(baseline_df, df[columns_to_merge], on='job_id')
    
    energy_consumed = merged_df['energy_y'].sum()
    energy_consumed_list.append(energy_consumed)
    print(f"Energy_consumed: {energy_consumed}")
    print("\n")
    
    IDLE_job_id = 1000000
    merged_df = merged_df[merged_df['job_id'] != IDLE_job_id]
    
    avg_run_time = merged_df['run_time_x'].mean()
    avg_wait_time = merged_df['wait_time_x'].mean()
    avg_response_time = merged_df['response_time_x'].mean()
    
    print(f"------------average_Run_time_Baseline: {avg_run_time}")
    print(f"------------average_Wait_time_Baseline: {avg_wait_time}")
    print(f"------------average_Response_time_Baseline: {avg_response_time}")
    print("\n")
    
    avg_run_time = merged_df['run_time_y'].mean()
    avg_wait_time = merged_df['wait_time_y'].mean()
    avg_response_time = merged_df['response_time_y'].mean()
    
    print(f"------------average_Run_time: {avg_run_time}")
    print(f"------------average_Wait_time: {avg_wait_time}")
    print(f"------------average_Response_time: {avg_response_time}")
    print("\n")
    
    run_time_percentage_increase = ((merged_df['run_time_y'].sum() - merged_df['run_time_x'].sum()) / merged_df['run_time_x'].sum()) * 100
    wait_time_percentage_increase = ((merged_df['wait_time_y'].sum() - merged_df['wait_time_x'].sum()) / merged_df['wait_time_x'].sum()) * 100
    response_time_percentage_increase = ((merged_df['response_time_y'].sum() - merged_df['response_time_x'].sum()) / merged_df['response_time_x'].sum()) * 100
    energy_consumed_percentage_increase = ((merged_df['energy_y'].sum() - merged_df['energy_x'].sum()) / merged_df['energy_x'].sum()) * 100
    wait_time_increase = (merged_df['wait_time_y'] - merged_df['wait_time_x']).mean()
    
    run_time_percentage_increase_list.append(run_time_percentage_increase)
    wait_time_percentage_increase_list.append(wait_time_percentage_increase)
    response_time_percentage_increase_list.append(response_time_percentage_increase)
    energy_consumed_percentage_increase_list.append(energy_consumed_percentage_increase)
    wait_time_increase_list.append(wait_time_increase)


#"wait_time_increase_seconds":[i for i in wait_time_increase_list],
plotdatadf = pd.DataFrame({"run_time_percentage_increase":[i for i in run_time_percentage_increase_list], 
                         "wait_time_percentage_increase":[i for i in wait_time_percentage_increase_list],
                         "response_time_percentage_increase":[i for i in response_time_percentage_increase_list],
                         "energy_consumed_percentage_increase":[i for i in energy_consumed_percentage_increase_list]}, index=file_names_list[1:])

formatted_plotdatadf = tabulate(plotdatadf, headers='keys', tablefmt='orgtbl', numalign='center')
print(formatted_plotdatadf)
print("\n")

# Applying formatting to all columns
for column in plotdatadf.columns:
    plotdatadf[column] = plotdatadf[column].apply(lambda x: "{:.4f}".format(x))

# Save DataFrame to a CSV file
plotdatadf.to_csv('output.csv', index=True)
print("percentage_increase saved to 'output.csv'")
print("\n")


energy_df = pd.DataFrame({"energy_consumed":[i for i in energy_consumed_list]}, index=file_names_list[1:])

energy_df.to_csv('energy_consumed.csv', index=True)
print("energy_consumed saved to 'energy_consumed.csv'")
print("\n")

# prints:
#print(df1.head(10))
#print(df2.head(10))
#print(df9.head(10))
#print("\n")

'''
# Use part of the data
sub_dfs_list = [df.iloc[:50] for df in dfs]

title_list = ['average power', 'run_time', 'wait_time', 'response_time']

for title in title_list:
    # Step 3: Plot Data
    plt.figure(figsize=(8, 10))
    plt.suptitle(f'{title}')
    # Width of the bars
    bar_width = 0.35

    plot_rows = len(dfs) - 1
    plot_columns = 1

    sub_df_baseline = sub_dfs_list[0]

    for sub_df in sub_dfs_list[1:]:
        for i in range(1, len(dfs)):
            plt.subplot(plot_rows, plot_columns, i)            
            plt.bar(sub_df['job_id'] - bar_width/2, sub_df_baseline[title], bar_width, color='blue')
            plt.bar(sub_df['job_id'] + bar_width/2, sub_df[title], bar_width, color='orange')
            # Add labels and title
            plt.xticks(sub_dfs_list[0]['job_id'], rotation=90)
            plt.xlabel('job_id')
            plt.ylabel(title)
            #plt.title(title)

    # Add legend
    lines = ["blue", "orange"] 
    labels = ["Baseline", f'{title}'] 
    plt.legend(lines, labels=labels, loc='upper right')
    

    # Show plot
    plt.tight_layout()
    plt.savefig(f'plot_{title}.png')
'''
