


EAR_DIR = /home/void/git/ear_private/
export CC = gcc
export SRCDIR = $(EAR_DIR)/src
export DB_LDFLAGS = -lmysqlclient

all:
	@ $(MAKE) -C 'src' all

clean:
	@ $(MAKE) -C 'src' clean
